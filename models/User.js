const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		// Requires the data for this our fields/properties to be included when creating a record.
		// The "true" value defines if the field is required or not and the second element in the array is the message that will be printed out in our terminal when the data is not present
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	totalAmount : {
		type: Number,
	},
	purchases : [
		{
			purchasedOn : {
				type : Date,
				default : new Date()
			},
			productId: {
           		type: String,
          		},
          	price: {
           		type: Number,
          				},
          	productName: {
          		type: String,
          	},
          	quantity: {
            	type: Number,
            	// required: true
          				},

			subtotal: {
				type: Number,
			}
		}
	]
});

// Models use Schemas and they act as the middleman from the server (JS code) to our database
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
module.exports = mongoose.model("User", userSchema);